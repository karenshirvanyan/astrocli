FROM quay.io/astronomer/ap-airflow:2.2.5-onbuild
ENV AIRFLOW__SECRETS__BACKEND="airflow.providers.hashicorp.secrets.vault.VaultBackend"
ENV AIRFLOW__SECRETS__BACKEND_KWARGS='{"connections_path": "connections", "variables_path": null, "config_path": null, "url": "http://vault:8200", "token":"mytoken"}'
ENV AIRFLOW__CORE__HIDE_SENSITIVE_VAR_CONN_FIELDS="False"
