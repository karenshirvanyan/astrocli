from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.operators.python import PythonOperator
from datetime import datetime
from airflow.hooks.base_hook import BaseHook
from airflow.models import Variable
from airflow.providers.google.cloud.transfers.gcs_to_bigquery import GCSToBigQueryOperator


def get_conn(**kwargs):
    conn = BaseHook.get_connection(kwargs['my_conn_id'])
    print("conn is equal to {}".format(conn.password))
    print(f"Password: {conn.password}, Login: {conn.login}, URI: {conn.get_uri()}, Host: {conn.host}")

def get_var(**kwargs):
    print(f"{kwargs['var_name']} {Variable.get(kwargs['var_name'])}")
    

with DAG('example_secrets_dag', start_date=datetime(2021, 1, 1), schedule_interval=None) as dag:

    start = DummyOperator(
        task_id='start'
    )

    conn_task = PythonOperator(
        task_id='conn-task',
        python_callable=get_conn,
        op_kwargs={'my_conn_id': 'smtp_default'},
    )

    start >> [conn_task]